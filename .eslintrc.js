module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es6: true,
        jest: true,
    },
    extends: 'eslint:recommended',
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
        process: 'readonly',
        page: true,
        browser: true,
        context: true,
        jestPuppeteer: true,
    },
    parserOptions: {
        ecmaVersion: 2018,
    },
    rules: {
    }
};