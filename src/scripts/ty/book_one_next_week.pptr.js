const isFinite = require('lodash/isFinite');
const { waitForChildFrame } = require('../../utils/waitForIChildFrame');
const { resolveAt } = require('../../utils/resolveAt');

const {
  TY_URL,
  TY_USERNAME,
  TY_PASSWORD,
  TY_LOCATION,
  TY_CLASSROOM,
  TY_CLASSNAME,
  TY_THIS_WEEK,
  TY_WAIT_UNTIL_TST_HHMM,
  TY_CLASS_CTRL_F_RANK: TY_CLASS_CTRL_F_RANK_STR,
} = process.env;

const TY_CLASS_CTRL_F_RANK = parseInt(TY_CLASS_CTRL_F_RANK_STR);
let TY_WAIT_UNTIL = null;
const [TY_WAIT_UNTIL_HH, TY_WAIT_UNTIL_MM] = TY_WAIT_UNTIL_TST_HHMM.split(':').map(n => parseInt(n));
if (isFinite(TY_WAIT_UNTIL_HH) && isFinite(TY_WAIT_UNTIL_MM)) {
  TY_WAIT_UNTIL = new Date();
  TY_WAIT_UNTIL.setUTCHours((TY_WAIT_UNTIL_HH - 8) % 24);
  TY_WAIT_UNTIL.setUTCMinutes(TY_WAIT_UNTIL_MM);
  TY_WAIT_UNTIL.setUTCSeconds(0);
}
const NS = './screenshots/ty-book_one_next_week';
let frame;

describe('訂課', () => {
  it('進入登入頁', async () => {
    await page.setViewport({ width: 990, height: 800 });
    await page.goto(TY_URL);
    await expect(page).toMatch('Class Booking System');
  });

  it('成功登入', async () => {
    await expect(page).toFillForm('form[name="aspnetForm"]', {
      ctl00$cphContents$txtUsername: TY_USERNAME,
      ctl00$cphContents$txtPassword: TY_PASSWORD,
    });
    await Promise.all([
      page.waitForNavigation(),
      expect(page).toClick('input[name="ctl00$cphContents$btnLogin"]'),
    ]);
    await expect(page).toMatch('Book for Class');
  });

  it('前往「Book for Class」頁面', async () => {
    await Promise.all([
      page.waitForNavigation(),
      expect(page).toClick('a', { text: 'Book for Class' }),
    ]);
  });

  it(`選擇會館「${TY_LOCATION}」`, async () => {
    await Promise.all([
      expect(page).toClick('a', { text: TY_LOCATION }),
      page.waitForNavigation(),
    ]);
  });

  if (!TY_THIS_WEEK) {
    it('選擇「NEXT WEEK」', async () => {
      await Promise.all([
        expect(page).toClick('a', { text: 'Next Week' }),
        page.waitForNavigation(),
      ]);
    });
  }

  it(`選擇教室「${TY_CLASSROOM}」`, async () => {
    await Promise.all([
      expect(page).toClick('label', { text: TY_CLASSROOM }),
      page.waitForNavigation(),
    ]);
  });

  it.skip('課程表截圖', async () => {
    await page.screenshot({
      path: `${NS}-${new Date()}-課程表-${TY_LOCATION}-NEXT_WEEK-${TY_CLASSROOM}.png`,
      fullPage: true
    });
  });

  it(`選擇頁面上第 ${TY_CLASS_CTRL_F_RANK} 堂「${TY_CLASSNAME}」`, async () => {
    const foundClasses = await page.$x(`//span[contains(text(), '${TY_CLASSNAME}')]`);
    expect(foundClasses.length).toBeGreaterThanOrEqual(TY_CLASS_CTRL_F_RANK)
    const targetClass = foundClasses[TY_CLASS_CTRL_F_RANK - 1];
    console.log(`TY_WAIT_UNTIL=${TY_WAIT_UNTIL}`);
    if (TY_WAIT_UNTIL instanceof Date) {
      await resolveAt(TY_WAIT_UNTIL);
    }
    await targetClass.click();
    await expect(page).toMatch('Class Info.', { timeout: 2000 }) // Dialog content
  });

  it('現在可以訂這堂課', async () => {
    frame = await waitForChildFrame({ page, urlContains: '/class-info.aspx' });
    expect(frame).toBeDefined();

    await expect(frame).not.toMatch('You are booked for this class.'); // 你已經訂到了
    await expect(frame).not.toMatch('Booking Start Date:'); // 尚未開放預定
    await expect(frame).not.toMatch('Online booking is already closed.'); // 上課前一小時關閉網路預訂
    await expect(frame).not.toMatch('Please logout then try to login again.'); // 登入後閒置太久
    await expect(frame).toMatch('BOOK THIS CLASS NOW');
  });

  it('按下「BOOK THIS CLASS NOW」', async () => {
    await Promise.all([
      expect(frame).toClick('a', { text: 'BOOK THIS CLASS NOW' }),
      page.waitForNavigation({ timeout: 90000 }),
    ]);
  });

  it('結束截圖', async () => {
    await page.screenshot({
      path: `${NS}-${new Date()}-結束.png`,
      fullPage: true
    });
  });

  it.skip('Wait 50s for manuel take-over', async () => {
    await jestPuppeteer.debug();
  });
});
