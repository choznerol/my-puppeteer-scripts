// 預設 timeout 為 10 分鐘
const DEFAULT_TIMEOUT = 600;

// 預設 pooling 間隔為 0.3 秒
const DEFAULT_POOLING_INTERVAL = 0.3;

/**
 * 指定在 `when` 時才 resolve 的 `promise`
 *
 * @param {Date} when 想要 resolve 的時刻
 * @param {Integer=600} timeout 檢查多久後放棄，單位為秒
 * @param {Integer=0.3} poolingInterval 多久檢查一次 `when`，單位為秒
 * @returns {Promise<void>} `when` 到時 resolve，如果經過 `timeout` 都還沒到就 reject
 */
function resolveAt(when, timeout = DEFAULT_TIMEOUT, poolingInterval = DEFAULT_POOLING_INTERVAL) {
  const timeoutMs = timeout * 1000;
  const poolingIntervalMs = poolingInterval * 1000;

  return new Promise(function scheduleOrTimeoutReachedPromise(resolve, reject) {
    if (!(when instanceof Date)) {
      reject(new Error(`${when} should be a Date!`));
    }

    const eta = when.getTime() - new Date().getTime();
    if (eta > timeoutMs) {
      reject(new Error(`[${new Date()}] ${when} 預計在 ${eta}ms 後發生，可是 ${timeoutMs}ms 後就會先 timeout 了`));
    }

    // Timeout
    setTimeout(() => reject(new Error(`[${new Date()}] Timeout ${timeout} reached before ${when}!`)), timeoutMs);

    function checkTime() {
      const eta = when.getTime() - new Date().getTime();
      console.log(`[${new Date}] ETA=${eta}ms checkTime(${when})`);

      if (eta <= 0) {
        resolve();
      } else {
        setTimeout(checkTime, poolingIntervalMs);
      }
    }

    checkTime();
  })
}

module.exports = { resolveAt };
