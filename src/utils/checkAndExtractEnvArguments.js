const checkAndExtractEnvArguments = (ARGV) => {
  Object.keys(ARGV).forEach(name => {
    const value = process.env[name];
    expect(value).toBeDefined();
    ARGV[name] = value;
  });
  return ARGV;
}

module.exports = { checkAndExtractEnvArguments };
