// 預設等待 popup 20秒 (40 * 500 = 20000ms)
async function waitForChildFrame({ page, urlContains, maxTry = 40, throttleMs = 500 }) {
  let found = null;
  let tired = 0;
  while (!found && tired < maxTry) {
    for (const child of page.mainFrame().childFrames()) {
      if (child.url().includes(urlContains)) {
        found = child;
        console.log(`Found ${urlContains} after ${tired * throttleMs}ms`)
      }
    }
    tired++;
    await page.waitFor(throttleMs);
  }
  return found;
}

module.exports = { waitForChildFrame };
