function screenshotFactory({ page, pathPrefix }) {
  return async function screenshot(args = { path: new Date()}) {
    page.screenshot({
      ...args,
      path: `${pathPrefix}${args.path}`,
    })
  }
}

module.exports = {
  screenshotFactory,
}