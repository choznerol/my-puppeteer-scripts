module.exports = {
  launch: {
    dumpio: true,
    headless: process.env.HEADLESS === 'true',
  },
  browser: 'chromium',
  browserContext: 'default',
}
