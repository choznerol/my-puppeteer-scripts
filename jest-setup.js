const { setDefaultOptions } = require('expect-puppeteer');
const dotenv = require('dotenv');

// Assign `/.env` onto `process.env`
dotenv.config()

// jest-puppeteer 的 matcher（e.g. toClick、toMatch）的最長等待時間偶而會不夠（預設 500ms）
// 詳見 jest-puppeteer 文件： https://github.com/smooth-code/jest-puppeteer/blob/master/packages/expect-puppeteer/README.md#configure-default-options
setDefaultOptions({ timeout: 1000 });

// 每個 it()、test()、before()、after() 的最長執行時間（預設 5000ms 對 E2E 太少）
// 由於我們用 `resolveAt` 來等待整點，其 timeout 是 10 分鐘，因此設為稍長一點的 11 分鐘
jest.setTimeout(11 * 60 * 1000);
